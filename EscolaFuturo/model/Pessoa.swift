//
//  Pessoa.swift
//  EscolaFuturo
//
//  Created by Jhonathan Mattos on 17/06/21.
//

import Foundation

enum Genero: String {
    
    case masculino = "masculino"
    case feminino = "feminino"
    case outro = "outro"
}


class Pessoa {
    
    var nome: String?
    var sobrenome: String?
    var idade: Int?
    var genero: Genero?
    var avatar: String?
    var CustomID: Int?
    
    init(nome: String?, sobrenome: String?, idade: Int?, genero: Genero?, avatar: String?,CustomID: Int?){
        
        
        self.nome = nome
        self.sobrenome = sobrenome
        self.idade = idade
        self.genero = genero
        self.avatar = avatar
        self.CustomID = CustomID
    }
    
}
