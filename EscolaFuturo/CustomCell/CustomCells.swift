//
//  CustomCells.swift
//  EscolaFuturo
//
//  Created by Jhonathan Mattos on 17/06/21.
//

import UIKit


protocol CustomCellsProtocol: AnyObject {
    
    func tapInEditButton(value: Pessoa)
}


class CustomCells: UITableViewCell {
    
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var perfilImage: UIImageView!
    private var pessoa: Pessoa?
    
    weak var delegate: CustomCellsProtocol?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setUp(value: Pessoa){
        self.pessoa = value
        self.nomeLabel.text = value.nome
        self.subTitleLabel.text = value.sobrenome
        self.perfilImage.image = UIImage(named: value.avatar ?? "")
    }
    
    
    @IBAction func tapInEditButton(_ sender: Any) {
        
        if let _pessoa = self.pessoa{
            self.delegate?.tapInEditButton(value: _pessoa)            
        }        
        print ("Button got Tapped")
    }
}
