//
//  EditDetailViewController.swift
//  EscolaFuturo
//
//  Created by Jhonathan Mattos on 22/06/21.
//

import UIKit



protocol EditDetailViewControllerProtocol: AnyObject {
    
    func upDatePessoa(value:Pessoa)
    
}

class EditDetailViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    
    @IBOutlet weak var editDetailImageView: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var nomeTextField: UITextField!
    @IBOutlet weak var sobrenomeTextField: UITextField!
    @IBOutlet weak var idadeTextField: UITextField!
    @IBOutlet weak var generoTextField: UITextField!
    
    weak var delegate: EditDetailViewControllerProtocol?
    
    var pessoaSelecionada: Pessoa?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenClickingScreen()
        
        
        self.addButtonOnKeyBoard()
        
        if let _pessoaSelecionada = self.pessoaSelecionada {
            
            
            self.editDetailImageView.image = UIImage (named: _pessoaSelecionada.avatar ?? "")
            self.nomeTextField.text = _pessoaSelecionada.nome
            self.sobrenomeTextField.text = _pessoaSelecionada.sobrenome
            self.generoTextField.text = _pessoaSelecionada.genero?.rawValue
            self.idadeTextField.text = String(_pessoaSelecionada.idade ?? 0)
            
        }
        
        self.nomeTextField.delegate = self
        self.sobrenomeTextField.delegate = self
        self.generoTextField.delegate = self
        self.idadeTextField.delegate = self
        
        
        // Do any additional setup after loading the view.
        
        self.nomeTextField.becomeFirstResponder()
    }
    
    
    
    func hideKeyboardWhenClickingScreen() {
        NotificationCenter.default.addObserver (self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver (self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        //Verifica se o usuário clicou fora da área do teclado para esconder o teclado
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    //Método chamado quando um clique é feito fora da área do teclado
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc  func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    
    @IBAction func editButtonClicked(_ sender: Any) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage {
            //imageView.contentMode = .scaleAspectFit
            editDetailImageView.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension EditDetailViewController: UITextFieldDelegate {
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
//        switch textField {
//        case self.nomeTextField:
//            self.sobrenomeTextField.becomeFirstResponder()
//        case self.sobrenomeTextField:
//            self.idadeTextField.becomeFirstResponder()
//        case self.idadeTextField:
//            self.generoTextField.becomeFirstResponder()
//        default:
//            textField.resignFirstResponder()
//        }
        
        
                if textField.isEqual(self.nomeTextField) {
        
                    self.sobrenomeTextField.becomeFirstResponder()
                }
                else if textField.isEqual(self.sobrenomeTextField) {
        
                    self.idadeTextField.becomeFirstResponder()
                }
                else if textField.isEqual(self.idadeTextField){
        
                    self.generoTextField.becomeFirstResponder()
                }
                else {
        
                    self.generoTextField.resignFirstResponder()
                }
        
        
                textField.resignFirstResponder()
        
        
        if let _pessoaSelecionada = self.pessoaSelecionada {
            
            self.delegate?.upDatePessoa(value: _pessoaSelecionada)
            
        }
        
        return true
    }
    
    private func addButtonOnKeyBoard() {
        let toolBar: UIToolbar = UIToolbar(frame:CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        
        toolBar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector (self.doneButtonTapped))
        
        let items = [flexSpace,doneButton]
        
        toolBar.items = items
        toolBar.sizeToFit()
        
        self.idadeTextField.inputAccessoryView = toolBar
    }
    
    @objc func doneButtonTapped(){
        print ("Done Button Tapped")
        
        if self.idadeTextField.isEqual(self.idadeTextField){
            
            self.generoTextField.becomeFirstResponder()
        }
        else {
            
            self.generoTextField.resignFirstResponder()
        }
    }
    
     func  textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField {
        
        case self.nomeTextField:
            self.pessoaSelecionada?.nome = textField.text
            
        case self.sobrenomeTextField:
            self.pessoaSelecionada?.sobrenome = textField.text
            
        case self.idadeTextField:
            self.pessoaSelecionada?.idade = Int (textField.text ?? "")
            
        default:
            self.pessoaSelecionada?.genero = Genero(rawValue: textField.text ?? "")
            
        }
    }
    
}




