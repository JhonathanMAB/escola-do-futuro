//
//  ViewController.swift
//  EscolaFuturo
//
//  Created by Jhonathan Mattos on 17/06/21.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var myTableView: UITableView!
    
    var arrayPessoas: [Pessoa] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        self.myTableView.register (UINib(nibName: "CustomCells", bundle: nil), forCellReuseIdentifier: "CustomCells")
        
        
        let pessoa1 = Pessoa(nome: "Jhonathan", sobrenome: "Belchior", idade: 26, genero: Genero.masculino, avatar:  "image1", CustomID: 1)
        let pessoa2 = Pessoa(nome: "Juliana", sobrenome: "Magalhaes", idade: 31, genero: Genero.feminino, avatar: "image2", CustomID: 2)
        let pessoa3 = Pessoa(nome: "Jeniffer", sobrenome: "Belchior", idade: 32, genero: Genero.feminino, avatar: "image3", CustomID: 3)
        let pessoa4 = Pessoa(nome: "Joelza", sobrenome: "Matos", idade: 52, genero: Genero.feminino, avatar: "image4", CustomID: 4)
        let pessoa5 = Pessoa(nome: "Eduardo", sobrenome: "Bernardi", idade: 27, genero: Genero.masculino, avatar: "image5", CustomID: 5)
        let pessoa6 = Pessoa(nome: "Caio", sobrenome: "Miranda", idade: 29, genero: Genero.masculino, avatar: "image1", CustomID: 6)
        let pessoa7 = Pessoa(nome: "Alexandre", sobrenome: "Matos", idade: 47, genero: Genero.masculino, avatar: "image2", CustomID: 7)
        let pessoa8 = Pessoa(nome: "Pietra", sobrenome: "Setubal", idade: 23, genero: Genero.outro, avatar: "image3", CustomID: 8)
        let pessoa9 = Pessoa(nome: "Felipe", sobrenome: "Miranda", idade: 31, genero: Genero.masculino, avatar: "image4", CustomID: 9)
        let pessoa10 = Pessoa(nome: "Fernando", sobrenome: "Douglas", idade: 30, genero: Genero.masculino, avatar: "image5", CustomID: 10)
        
        
        self.myTableView.delegate = self
        self.myTableView.dataSource = self
        
        
        arrayPessoas =  [pessoa1, pessoa2, pessoa3, pessoa4, pessoa5, pessoa6, pessoa7, pessoa8, pessoa9, pessoa10]
        
        //        let pessoas: Pessoa = Pessoa(nome: "Jhonathan", sobrenome: "Belchior", idade: 26, genero: Genero.masculino)
        
    }
    
}





//MARK: - ViewController
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayPessoas.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CustomCells? = tableView.dequeueReusableCell(withIdentifier: "CustomCells", for: indexPath) as? CustomCells
        
        cell?.setUp(value: self.arrayPessoas[indexPath.row])
        cell?.delegate = self
        
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let pessoaSelecionada: Pessoa  = self.arrayPessoas[indexPath.row]
        
        performSegue(withIdentifier: "DetailViewController", sender: pessoaSelecionada)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "EditDetailViewController" {
            let vc:EditDetailViewController? = segue.destination as? EditDetailViewController
            vc?.pessoaSelecionada = sender as? Pessoa
            vc?.delegate = self
        }
        else {
            let vc:DetailViewController? = segue.destination as? DetailViewController
            vc?.pessoaSelecionada = sender as? Pessoa
            
        }
    }
    
}

//MARK: - CustomCellsProtocol
extension ViewController: CustomCellsProtocol {
    
    func tapInEditButton(value: Pessoa) {
        print ("ViewController")
        
        self.performSegue(withIdentifier: "EditDetailViewController", sender: value)
        
    }
}


//MARK: - EditDetailViewControllerProtocol
extension ViewController: EditDetailViewControllerProtocol {
    func upDatePessoa(value: Pessoa) {
        
//        edicao manual =
//        self.arrayPessoas = self.arrayPessoas.filter({$0.CustomID != value.CustomID})
//        self.arrayPessoas.append(value)
        self.myTableView.reloadData()
        
        print("upDatePessoa")
    }
}
