//
//  DetailViewController.swift
//  EscolaFuturo
//
//  Created by Jhonathan Mattos on 22/06/21.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var sobrenomeLabel: UILabel!
    @IBOutlet weak var idadeLabel: UILabel!
    @IBOutlet weak var generoLabel: UILabel!
    
    var pessoaSelecionada: Pessoa?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.detailImageView.image = UIImage (named: self.pessoaSelecionada?.avatar ?? "")
        self.nomeLabel.text = self.pessoaSelecionada?.nome
        self.sobrenomeLabel.text = self.pessoaSelecionada?.sobrenome
        self.generoLabel.text = self.pessoaSelecionada?.genero?.rawValue
        self.idadeLabel.text = String(self.pessoaSelecionada?.idade ?? 0)

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
